package ventana;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class ventana extends JFrame {
	private JPanel panel;
	private JButton boton;
	/*
	 * @param valores sirve para represtar donde debe ir cada boton y su valor
	 */
	private String valores [][]= {{"7","8","9","/"},
						  		  {"4","5","6","X"},
						  		  {"1","2","3","-"},
						  		  {",","0","=","+"}};
	
	public ventana() {
		crearPanel();
		aņadirBotones();
		this.setVisible(true);
	}
	
	
	
	private void aņadirBotones() {
		
		int columna; 
		int fila;  
		for (int i = 0; i < valores.length; i++) {
			for (int j = 0; j < valores[i].length; j++) {
				boton = new JButton(valores[i][j]);
				fila = (i*50)+50;
				columna = j*50;
				boton.setBounds(columna, fila,50, 50);
				panel.add(boton);
			}
		}	
	}
	
	private void crearPanel() {
		this.setSize(217, 290);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Calculadora");
		this.setLocationRelativeTo(null);
		panel = new JPanel();
		panel.setLayout(null);
		this.getContentPane().add(panel);
	}
}