package test;

import static org.junit.jupiter.api.Assertions.*;
import calculos.*;

import org.junit.jupiter.api.Test;

public class test_operaciones {
	/*
	 * @param a es valor predeterminado para las pruebas
	 * @param b es valor predeterminado para las pruebas
	 */
	
	int a = 5;
	int b = 10;

	@Test
	void testsuma1() {
		assertEquals(4, operaciones.suma(a,b));
	}
	@Test
	void testsuma2() {
		assertEquals(15, operaciones.suma(a,b));
	}
	
	@Test
	void testresta1() {
		assertEquals(4, operaciones.resta(a,b));
	}
	
	@Test
	void testresta2() {
		assertEquals(-5, operaciones.resta(a,b));
	}
	
	@Test
	void testmultiplicar1() {
		assertEquals(4, operaciones.multiplicar(a,b));
	}
	@Test
	void testmultiplicar2() {
		assertEquals(50, operaciones.multiplicar(a,b));
	}
	
	@Test
	void testdividir() {
		assertEquals(4, operaciones.division(a,b));
	}
	@Test
	void testdivdir() {
		assertEquals(0, operaciones.division(a,b));
	}


}
 