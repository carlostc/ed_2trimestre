package calculos;

public class operaciones {
	
	/*
	 * @param a es el primer valor de la suma
	 * @param b es el segundo valor de la suma
	 * @return devuelve suma
	 */
	
	public static  int suma(int a, int b) {
		return a+b;
	}
	
	/*
	 * @param a es el primer valor de la resta
	 * @param b es el segundo valor de la resta
	 * @return devuelve resta
	 */
	
	public static int resta(int a, int b) {
		return a-b;
	}
	
	/*
	 * @param a es el primer valor de la dividendo
	 * @param b es el segundo valor de la divisor
	 * @return devuelve la division
	 */
	
	public static int division(int a, int b) {
		return a/b;
	}
	
	/*
	 * @param a es el primer valor de la multiplicacion
	 * @param b es el segundo valor de la multiplicacion
	 * @return devuelve  multiplicacion
	 */
	public static int multiplicar(int a, int b) {
		return a*b;
	}
}
